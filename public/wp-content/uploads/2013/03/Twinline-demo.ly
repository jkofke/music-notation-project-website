\version "2.12.3"

%This is as close as we get to Twinline at the moment...

%Begin note head style customization code
%Association list of pitches to note head styles.
%Grouped by pitch. Defaults: 0 is C, 1 is D, ... 6 is B.
% note head styles: "do re mi" (shape note) or "cross, mensural, default" etc.
#(define style-mapping
   (list
    (cons (ly:make-pitch 0 6 SHARP) 'default)
    (cons (ly:make-pitch 0 0 NATURAL) 'default)
    (cons (ly:make-pitch 0 1 DOUBLE-FLAT) 'default)

    (cons (ly:make-pitch 0 6 DOUBLE-SHARP) 'do)
    (cons (ly:make-pitch 0 0 SHARP) 'do)
    (cons (ly:make-pitch 0 1 FLAT) 'do)

    (cons (ly:make-pitch 0 0 DOUBLE-SHARP) 'default)
    (cons (ly:make-pitch 0 1 NATURAL) 'default)
    (cons (ly:make-pitch 0 2 DOUBLE-FLAT) 'default)

    (cons (ly:make-pitch 0 1 SHARP) 'cross)
    (cons (ly:make-pitch 0 2 FLAT) 'cross)
    (cons (ly:make-pitch 0 3 DOUBLE-FLAT) 'cross)

    (cons (ly:make-pitch 0 1 DOUBLE-SHARP) 'default)
    (cons (ly:make-pitch 0 2 NATURAL) 'default)
    (cons (ly:make-pitch 0 3 FLAT) 'default)

    (cons (ly:make-pitch 0 2 SHARP) 'do)
    (cons (ly:make-pitch 0 3 NATURAL) 'do)
    (cons (ly:make-pitch 0 4 DOUBLE-FLAT) 'do)

    (cons (ly:make-pitch 0 2 DOUBLE-SHARP) 'default)
    (cons (ly:make-pitch 0 3 SHARP) 'default)    
	(cons (ly:make-pitch 0 4 FLAT) 'default)

    (cons (ly:make-pitch 0 3 DOUBLE-SHARP) 'cross)
    (cons (ly:make-pitch 0 4 NATURAL) 'cross)
    (cons (ly:make-pitch 0 5 DOUBLE-FLAT) 'cross)

    (cons (ly:make-pitch 0 4 SHARP) 'default)
    (cons (ly:make-pitch 0 5 FLAT) 'default)

    (cons (ly:make-pitch 0 4 DOUBLE-SHARP) 'do)
    (cons (ly:make-pitch 0 5 NATURAL) 'do)
    (cons (ly:make-pitch 0 6 DOUBLE-FLAT) 'do)

    (cons (ly:make-pitch 0 5 SHARP) 'default)
    (cons (ly:make-pitch 0 6 FLAT) 'default)
    (cons (ly:make-pitch 0 0 DOUBLE-FLAT) 'default)

    (cons (ly:make-pitch 0 5 DOUBLE-SHARP) 'cross)
    (cons (ly:make-pitch 0 6 NATURAL) 'cross)
    (cons (ly:make-pitch 0 0 FLAT) 'cross)))

%Compare pitch and alteration (not octave).
#(define (pitch-equals? p1 p2)
   (and
    (= (ly:pitch-alteration p1) (ly:pitch-alteration p2))
    (= (ly:pitch-notename p1) (ly:pitch-notename p2))))

#(define (pitch-to-style pitch)
   (let ((style (assoc pitch style-mapping pitch-equals?)))
     (if style
         (cdr style))))

#(define (style-notehead grob)
   (pitch-to-style
    (ly:event-property (event-cause grob) 'pitch)))

%End notehead customization functions


scales = \relative c' {
  c cis d dis e f fis g gis a ais b c1 
  c,2 cis d dis e f fis g gis a ais b c1 
}


%{ Twinline style staff with 1-3-1-3... note positioning  
Note the special scheme function used for staffLineLayoutFunction
%}
\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #(lambda (p) (+ 1 (round (+ -1 (/ (ly:pitch-semitones p) 2)))))
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
  \override NoteHead #'style = #style-notehead
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}



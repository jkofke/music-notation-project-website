export const makeMetaTitle = (title: string, section?: string) =>
  section
    ? `${title} | ${section} | The Music Notation Project`
    : `${title} | The Music Notation Project`;

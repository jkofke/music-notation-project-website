---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Research Project", "MNMA")}
  metaDescription="Research project of the Music Notation Modernization Association (MNMA). Collected and evaluated alternative music notation systems. Concluded in 1999."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="page type-page status-publish hentry">
        <header class="entry-header">
          <h1 class="mnma-title">
            MNMA | Music Notation Modernization Association | 1985-2007
          </h1>
          <h1 class="entry-title">Research Project</h1>
        </header>

        <div class="entry-content">
          <p>
            The most ambitious goal of the <a href="/mnma/">MNMA</a> was to conduct
            evaluative research in an effort to determine which notation systems
            were most promising as potential replacements for traditional notation.
          </p>
          <p>
            In pursuit of this goal the MNMA carried out a research project that
            spanned several years and was concluded in 1999. It was organized
            into the following four steps.
          </p>
          <ul>
            <li><strong><a href="#one">Step 1 : Collection</a></strong></li>
            <li>
              <strong
                ><a href="#two">Step 2 : Eleven Preliminary Screens</a>
              </strong>
            </li>
            <li>
              <strong
                ><a href="#three"
                  >Step 3 : Six More Subjective and Detailed Screens</a
                >
              </strong>
            </li>
            <li>
              <strong
                ><a href="#four">Step 4 : Evaluation by Musicians</a>
              </strong>
            </li>
            <li><strong><a href="#five">Results in Brief</a></strong></li>
            <li>
              <strong
                ><a href="#six">Excerpt from the Report on the Results</a>
              </strong><strong></strong>
            </li>
            <li>
              <strong
                ><a href="#seven">Notation Systems Sorted by Average Score</a>
              </strong>
            </li>
          </ul>
          <h2 class="divider-heading" id="one">Step 1: Collection</h2>
          <p>
            Over 500 music notation systems, new and old, established or
            proposed, were collected from all over the world. They were then
            researched and documented in the book <a
              href="/mnma/publications/#proposals"
              >Directory of Music Notation Proposals</a
            >, by Thomas Reed.
          </p>
          <h2 class="divider-heading" id="two">
            Step 2: Eleven Preliminary Screens
          </h2>
          <p>
            A set of 11 criteria, or &#8220;screens&#8221; were adopted to
            establish the minimum standards that the music notations should meet
            in order to merit further consideration. Applying these screens
            winnowed the pool of hundreds of music notations down to a
            manageable 45.
          </p>
          <ol class="spacious-list">
            <li>
              The notation is convenient for a human writer (as contrasted with
              a machine) to express musical ideas. The notation is convenient
              for a human performer to recreate those musical ideas.
            </li>
            <li>
              The notation can be written conveniently and quickly with nothing
              more than a writing tool (such as a pencil) without the absolute
              necessity of a ruler or other drawing aids or specially prepared
              paper. In other words, a plain piece of paper and a pencil, or a
              chalkboard and chalk should be sufficient for quickly notating
              music in the notation if desired.
            </li>
            <li>
              The notation is independent of all musical instruments for
              intelligibility so that the notation is readily adaptable to all
              instruments including the human voice.
            </li>
            <li>
              The notation can express music of all reasonable degrees of
              complexity &#8211; not only simple music.
            </li>
            <li>
              The notation is relatively simple so as to be practical for both
              children and adults.
            </li>
            <li>
              The music is flexible enough so as to be appropriate for the music
              of the past, present, and foreseeable future, as well as to music
              of various cultures, and to both solo and ensemble performance.
            </li>
            <li>
              The notation is writable using only a single color on a
              contrasting background (for example black on white) without
              shading or tinting. Such a monochrome system offers the maximum in
              simplicity and convenience, and is considered essential,
              especially since many people have some degree of color-blindness.
            </li>
            <li>
              The notation possesses a fully proportional pitch coordinate,
              where each of the twelve common pitches is spaced in a graphic
              manner, so that progressively larger pitch intervals have
              progressively larger spacing on the coordinate, providing a visual
              representation of each interval that is exactly proportional to
              its actual sound.
            </li>
            <li>
              The staff, or graph, shows an octave cycling effect, or octave
              periodicity, so that each successive octave appears the same or
              substantially the same, making it possible to recognize notes in
              any register after learning one octave.
            </li>
            <li>
              No more than five identical, successive, and equidistant staff
              lines are shown, so that staff lines can be quickly identified
              without counting lines.
            </li>
            <li>
              Both the lines and spaces of a staff are used as positions for
              notes on the pitch coordinate in order to economize on paper space
              and therefore on eye movement.
            </li>
          </ol>
          <h2 class="divider-heading" id="three">
            Step 3: Six More Subjective and Detailed Screens
          </h2>
          <p>
            A second set of 6 more subjective and detailed screens were applied
            to the remaining 45 music notations, winnowing them down to the 37
            that would be evaluated by musicians in step four.
          </p>
          <ol class="spacious-list">
            <li>
              (12.) Adequate provision for voice leading (keeping multiple
              melodic lines distinct) is provided.
            </li>
            <li>
              (13.) The time coordinate must provide for proportional (or
              approximately proportional) graphic spacing of notes, rests, and
              other events, and must also provide for mathematically understood
              symbols for the divisions and multiples of time values, except
              optionally in children&#8217;s music and situations where graphic
              representation of time values alone may be adequate.
            </li>
            <li>
              (14.) The notation is adaptable to a variety of microtonal
              systems.
            </li>
            <li>
              (15.) The notation system must allow the pitch axis to be
              uninterrupted (made continuous) so that the staff can encompass an
              arbitrary number of octaves while preserving proportionality of
              pitch. In addition, the notation system must allow the pitch axis
              to be interrupted (made discontinuous) at convenient points in
              order to provide separate staves for specific instruments or
              voices in an ensemble, or to separate the two hands in keyboard
              music, when desired. Both options (continuous and discontinuous)
              must be available.
            </li>
            <li>
              (16.) The notation provides for the convenient addition of
              optional or supplementary kinds of information, which may or may
              not be necessary or desired in some music (for example,
              information about dynamics, expression, tonality, choice of
              instruments, tone color, lyrics, etc).
            </li>
            <li>
              (17.) Frequently used symbols must be at least as convenient to
              write in longhand as are the corresponding symbols of traditional
              notation. For example, if the noteheads are all rectangular, or
              require unusually precise drawing, they take an unacceptably long
              time to draw. Exceptions are allowed for symbols that provide some
              benefit missing from the traditional system, as long as the
              overall amount of time to write a typical piece of music is not
              noticeably longer than in traditional notation.
            </li>
          </ol>
          <h2 class="divider-heading" id="four">
            Step 4: Evaluation by Musicians
          </h2>
          <p>
            The final evaluation was completed by a group of seven trained
            musicians. They assessed the remaining 37 notation systems by
            conducting the following series of hands-on exercises. This step is
            further documented in the book <a href="/mnma/publications/#test"
              >Test for New Notation Systems</a
            > by Thomas Reed.
          </p>
          <ol class="spacious-list">
            <li>
              (18.-22.) Reading a chromatic scale for each system and grading
              each system according to 5 criteria (ease of identifying staff
              lines and spaces, ease of writing a note a Major 3rd above the top
              line of the staff and below the bottom line, ease of reading a
              piano staff version, ease of recognizing noteheads, ease of
              reading successive octaves)
            </li>
            <li>
              (23.-27) Writing out a G-minor scale for selected systems and
              grading each according to 5 criteria (ease of writing noteheads,
              ease of writing 3-octave range, ease of recognizing it as a
              melodic minor scale &#8211; as opposed to harmonic minor, pure
              minor, or major scale, ease of writing the time system, ease of
              selecting the best type of manuscript paper)
            </li>
            <li>
              (28.-32.) Writing out last 4 measures of a fugue (Bach&#8217;s
              Toccata and Fugue in D minor for organ, BWV 565) and grading
              selected systems according to 5 criteria (ease of selecting
              manuscript paper, ease of using voice leading indications &#8211;
              showing which notes belong to which voice even when crossing, ease
              of using &#8220;tie&#8221; symbols, ease of use by an organist,
              ease of designating use of left or right hand)
            </li>
            <li>
              (33.-38.) Writing out first 4 measures of Ravel&#8217;s
              D&#8217;anne qui me jecta de la neige (from Deux Epigrammes de
              Clement Marot for voice and piano) and grading selected systems
              according to 6 criteria (adequate expression of pitch information,
              adequate expression of time and note duration, ease for soloist
              and pianist, adequate expression of ornaments, ease of writing,
              adequate readability)
            </li>
            <li>
              (39.-44.) Writing out a specific measure from an orchestral work
              (measure 2 of rehearsal #161 from theSacrificial Dance of Igor
              Stravinsky&#8217;s Rite of Spring, Dover edition), and grading
              selected systems according to 6 criteria (adequate accommodation
              for 30 staves, ease of fitting the parts on the staves without
              using numerous ledger lines, degree of freedom from octave
              register changes or clef changes, legibility if photographically
              reduced, legibility of symbols, legibility of expression marks in
              blank paper space)
            </li>
          </ol>
          <h2 class="divider-heading" id="five">Results in Brief</h2>
          <p>
            This research project spanned many years. In summary, more than five
            hundred notations were collected and presented in the <a
              href="/mnma/publications/#proposals"
              >Directory of Music Notation Proposals</a
            >. Forty-five of these notations passed successfully through step
            two. Thirty-seven passed successfully through step three (see list
            of systems below). The responses from the seven musicians
            participating in step four were analyzed and presented in the
            &#8220;Report on the Results of the MNMA Evaluation Test.&#8221; An
            excerpt from this report is included below. The complete responses
            from each musician who participated in step four were subsequently
            published in full detail in various issues of <a
              href="/mnma/publications/#news">Music Notation News</a
            >, including illustrations of their transcriptions.
          </p>
          <p>
            Although the results were not as conclusive as had been hoped, much
            was learned from the study. There were two notation systems that
            were the most highly rated:
          </p>
          <ul class="spacious-list">
            <li>
              Thomas Reed&#8217;s <a
                href="/system/twinline-notation-by-thomas-reed/">Twinline</a
              >, a variant of Leo de Vries&#8217;s <a
                href="/system/chromatic-twinline-by-leo-de-vries/"
                >Chromatic Twinline</a
              > notation.
            </li>
            <li>
              Richard Parncutt&#8217;s <a
                href="/system/6-6-tetragram-by-richard-parncutt/"
                >6-6 Tetragram</a
              >, considered jointly with Albert Brennink&#8217;s <a
                href="/system/a-b-chromatic-notation-by-albert-brennink/"
                >A-B Chromatic Notation</a
              >, of which it is a minor variant.
            </li>
          </ul>
          <h2 class="divider-heading" id="six">
            Excerpt from the Report on the Results
          </h2>
          <p>
            <em
              >(The following is a short excerpt from the full &#8220;Report on
              the Results of the MNMA Evaluation Test&#8221; written by Doug
              Keislar. The full text includes statistical analyses of the
              participating musicians&#8217; responses and was published in the
              Vol. 10, No. 2, 2nd Quarter 2000 issue of the MNMA&#8217;s <a
                href="/mnma/publications/#news">Music Notation News</a
              >.)</em
            >
          </p>
          <p>
            If the evaluators had been close to unanimous in their opinions of
            the best systems, interpreting the results would be straightforward.
            As it turned out, they were far from unanimous, meaning that one
            must examine the results carefully before drawing conclusions. If
            one must choose an existing system based on the test results, with
            none of the improvements suggested by the evaluators (and not
            allowing for future proposals), I see two candidates, depending on
            the criteria one uses.
          </p>
          <p>
            Only one system was chosen as a final preferred system by two
            evaluators: LP #15, Parncutt 6-6 Tetragram. The fact that this
            system is a minor variant of LP#14, Brennink, which was chosen as a
            third evaluator&#8217;s final system, strengthens its position
            considerably. Taken together, Brennink&#8217;s system and
            Parncutt&#8217;s variation of it won three of the seven final
            &#8220;votes&#8221; by evaluators, which is a strong plurality.
            Unfortunately, these three evaluators were not wildly enthusiastic
            about the system. One rated it somewhat worse than traditional,
            another about the same, and the third somewhat better. This is not
            the kind of ringing endorsement that would convince the world at
            large to bother learning the system as an alternative to traditional
            notation, much less as a replacement for it.
          </p>
          <p>
            Only one system among the final choices received an evaluation of
            &#8220;much better than traditional&#8221;: LP#6, Tom Reed&#8217;s
            Twinline. This is the kind of ringing endorsement one would hope
            for, but unfortunately it came from only one out of seven
            evaluators, far from a consensus. However, if one examines the
            overall numerical results instead of only looking at the final
            choices, the position of LP#6 does become somewhat stronger: it
            receives the highest numerical rating in all the analyses of the
            scores, higher than LP#15 (or LP#14). The final report will include
            all evaluators&#8217; numerical responses to all questions in the
            test, so that readers can verify my analyses if desired.
          </p>
          <p>
            These two systems, LP#14/15 and LP#6, represent the work of two men
            who have each devoted innumerable hours to the problem of notation
            reform: Albert Brennink and Thomas Reed. (Parncutt&#8217;s
            contribution to Brennink&#8217;s system, while evidently deemed a
            worthy improvement by the evaluators, cannot be considered the
            product of decades of research in notation reform; so it seems fair
            to lump his system together with Brennink&#8217;s in the present
            discussion.) Certainly one cannot assert that either
            Brennink&#8217;s or Reed&#8217;s system is carelessly designed.
            Brennink&#8217;s system is closer in appearance to traditional
            notation; at a glance, it can actually be mistaken for traditional
            notation. Reed&#8217;s system, on the other hand, ostensibly avoids
            the problem that plagues almost all chromatic notation proposals:
            their less efficient use of space on the page.
          </p>
          <p>
            In short, I find that both the Brennink/Parncutt system and the Reed
            Twinline system have their advantages. The former system&#8217;s
            greater similarity in appearance to traditional notation would
            probably make it easier to promote. However, I also see advantages
            to some features of systems that no evaluators selected as a final
            choice. (For example, the use of alternating black and white
            noteheads would accentuate the staff lines and spaces, allowing
            music to be printed smaller, and it would also highlight intervallic
            patterns, facilitating transposition and improvisation.) Perhaps a
            composite system can be imagined. I think it likely that a notation
            system superior to any of the 37 studied in this test can, and
            eventually will, be invented. I will not predict whether the world
            at large will adopt such a system (or any of the existing ones); it
            might well be that traditional notation is too entrenched to be
            replaced, or even significantly displaced, in the foreseeable future
            (say, within the next century). Perhaps only a system that is an
            order of magnitude better than the ensconced system, not merely
            twice as good, can effect such a global change. One can, however,
            foresee that computers will make it much easier for experimentally
            minded people to adopt notation systems of their liking and to
            translate music between those systems and traditional notation.
          </p>
          <h2 class="divider-heading" id="seven">
            Notation Systems Sorted by Average Score
          </h2>
          <p>
            Below are the thirty-seven notation systems that passed the
            seventeen screens of steps two and three and were then evaluated by
            seven musicians in step four. They are sorted by their average score
            as awarded by the musician-evaluators, with the highest-rated
            systems shown first.
          </p>
          <p>
            The MNMA does not claim that this ordering should be considered
            authoritative, but it does provide a cursory look at the thinking of
            the seven evaluators. Note that all seven musicians were well-versed
            in traditional notation, which would inevitably have an impact on
            their impressions, as compared with beginners. For a more detailed
            and thorough look at the results we encourage those interested to
            consult the full “Report on the Results of the MNMA Evaluation Test”
            in the second quarter 2000 issue of <a
              href="/mnma/publications/#news">Music Notation News</a
            >.
          </p>
          <p>
            (A number of notation systems that are now presented on our website
            under <a href="/systems/">Notation Systems</a> were not included in this
            research because they were designed after it was completed in 1999 or
            were unknown to the MNMA at the time. (<a
              href="/system/muto-notation-by-muto-foundation/">MUTO</a
            > and <a href="/system/bilinear-notation-by-jose-a-sotorrio/"
              >Bilinear</a
            > fall into the latter category.) See <a href="/systems/gallery/"
              >Music Notations Sorted by Date</a
            > for more information.)
          </p>
          <p>
            Each of the images below shows a chromatic scale from C to C.  Click
            on the links to learn more about these notation systems.
          </p>
          <p>&nbsp;</p>
          <div class="system-container-mnma">
            <a href="/systems/group/major-3rd-2-lines-compact/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/twinline-tom-reed.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">1. Twinline by Thomas Reed</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/a-b-chromatic-notation-by-albert-brennink/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/a-b-chromatic-albert-brennink1.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >2. A-B Chromatic Notation by Albert Brennink</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/systems/group/whole-step-4-3-lines/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/6-6-tetragram-richard-parncutt.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >3. 6-6 Tetragram by Richard Parncutt</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/notagraph-by-constance-virtue/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/notagraph-constance-virtue.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >4. NotaGraph by Constance Virtue</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/da-notation-by-rich-reed/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/da-notation-rich-reed.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">5. DA Notation by Rich Reed</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/systems/group/whole-step-5-lines/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/5-line-chromatic-staff-heinrich-richter.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >6. Untitled by Heinrich Richter (one of several 5-line
                notations)</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-johann-ailler/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-johann-ailler.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">7. Untitled by Johann Ailler</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/c-symmetrical-semitone-notation/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/c-symmetrical-semitone-ronald-sadlier.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >8. C-Symmetrical Semitone Notation by Ronald Sadlier</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/systems/group/whole-step-5-lines/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/5-line-chromatic-staff-heinrich-richter.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >9-15. A set of seven 5-line notations with identical staves
                whose average scores were very close: Untitled by Hermann-Josef
                Wilberrt, Untitled by Lukas Brandt, Volksnotenschrift
                (People&#8217;s Notation) by Hans Sacher, Untitled by Kazimierz
                Wolf, A Simplified Music Notation by Edward V. Huntington,
                Untitled by Karl Bernhard Schumann, 12-tone Scale from 6 Lines
                by Heinrich Vincent</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/notation-godjevatz-by-velizar-godjevatz/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/notation-godjevatz-velizar-godjevatz.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >16. Notation Godjevatz by Velizar Godjevatz</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/diatonic-twinline-by-leo-de-vries/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/twinline-diatonic-leo-de-vries.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >17. Diatonic Twinline by Leo de Vries</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-grace-frix/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-grace-frix.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">18. Untitled by Grace Frix</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-franz-grassl/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-franz-grassl.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">19. Untitled by Franz Grassl</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/ambros-system-by-august-ambros/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/ambros-system-august-ambros.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >20. Ambros System by August Ambros</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/chromatic-twinline-by-leo-de-vries/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/twinline-chromatic-leo-de-vries.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >21. Chromatic Twinline by Leo de Vries</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/isomorph-notation-by-tadeusz-wojcik/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/isomorph-tadeusz-wojcik.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >22. Isomorph Notation by Tadeusz Wojcik</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/note-for-note-by-walter-h-thelwall/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/note-for-note-walter-thelwall.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >23. Note for Note by Walter H. Thelwall</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/0-5-7-notation-by-richard-parncutt/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/0-5-7-notation-richard-parncutt.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >24. 0-5-7 Notation by Richard Parncutt</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/douzave-system-by-john-leon-acheson/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/douzave-john-acheson.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >25. Douzave System by John Leon Acheson</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a
              href="/system/notation-for-the-system-of-equal-tones-by-gustave-decher/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/system-of-equal-tones-gustave-decher.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >26. Seven-tone or Twelve-tone Notation by Hans Krenn</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/panot-notation-by-george-skapski/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/panot-george-skapski.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >27. Panot Notation by George Skapski</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a
              href="/system/proportional-chromatic-musical-notation-by-henri-carcelle/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/proportional-chromatic-henri-carcelle.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >28. Proportional Chromatic Musical Notation by Henri Carcelle
                (a vertically oriented notation)</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a
              href="/system/notation-for-the-system-of-equal-tones-by-gustave-decher/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/system-of-equal-tones-gustave-decher.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >29. Notation for the System of Equal Tones by Gustave Decher</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-klaus-lieber/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-klaus-lieber.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">30. Untitled by Klaus Lieber</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/chromatic-6-6-notation-by-johannes-beyreuther/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/chromatic-66-johannes-beyreuther.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >31. Chromatic 6-6 Notation by Johannes Beyreuther</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/6-6-klavar-by-cornelis-pot/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/6-6-klavar-cornelis-pot-horizontal.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >32. 6-6 Klavar by Cornelis Pot (a vertically oriented notation)</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/avique-notation-by-anne-and-bill-collins/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/avique-anne-bill-collins.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >33. Avique Notation by Anne &amp; Bill Collins</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-arnold-schoenberg/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-arnold-schoenberg.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >34. Untitled by Arnold Schoenberg</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-johannes-beyreuther/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-johannes-beyreuther.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >35. Untitled by Johannes Beyreuther</span
              >
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-robert-stuckey/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-robert-stuckey.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title">36. Untitled by Robert Stuckey</span>
            </a>
          </div>
          <div class="system-container-mnma">
            <a href="/system/untitled-by-nicolai-dolmatov/"
              ><img
                alt=""
                src="/wp-content/uploads/2012/07/untitled-nicolai-dolmatov.png"
              /><img
                alt=""
                src="/wp-content/uploads/2012/08/chromatic-scale-caption-grey.png"
              /><span class="system-title"
                >37. Untitled by Nicolai Dolmatov</span
              >
            </a>
          </div>
          <p>&nbsp;</p>
        </div>
      </article>
    </div>
  </div>
</MnpLayout>

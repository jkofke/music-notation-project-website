---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Chromatic Staves Example", "Tutorials")}
  metaDescription="A musical passage that requires a complex key signature and many accidental signs in traditional notation, can be clarified by alternative notation systems."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Chromatic Staves Example</h1>
        </header>

        <div class="entry-content">
          <p>
            This tutorial focuses on the difficulties related to key signatures
            and accidentals in traditional notation. Let&#8217;s look at an
            excerpt from Franz Liszt&#8217;s &#8220;Hungarian Rhapsody 2.&#8221;
            This passage is in F# Major, one of the more difficult key
            signatures, and also entails the use of many accidentals.
          </p>
          <p>
            <img
              loading="lazy"
              alt="An excerpt from Franz Liszt's Hungarian Rhapsody 2"
              src="/wp-content/uploads/2013/03/LisztLine1FullTN.gif"
              width="675"
              height="227"
            />
          </p>
          <h2>
            An excerpt from Franz Liszt&#8217;s &#8220;Hungarian Rhapsody
            2&#8221;
          </h2>
          <p>
            If a picture is worth a thousand words, then this example certainly
            speaks for itself. For someone not already well-trained in
            traditional notation it is a difficult passage to read with its many
            accidentals combined with a difficult key signature.
          </p>
          <p>
            (Actually, experienced readers of traditional notation may not find
            this passage to be that difficult for reasons that will become
            apparent below. Nevertheless, consider how daunting it is for a
            beginner and how much training it takes for it to no longer seem so
            difficult.)
          </p>
          <p>
            There are also many other articulation marks vying for the
            musician&#8217;s attention. Just deciphering the pitch of the notes
            takes effort and attention that could be paid to the other
            articulation marks. This illustrates the general point that the
            difficulties of traditional notation take time and effort that could
            be better spent on other things such as musicality, expression, or
            technique. However, for the purposes of this tutorial, we have
            removed these other articulation marks from subsequent images so we
            can better focus on the accidentals and key signature.
          </p>
          <p>
            As many musicians have had to do with such passages, we can also
            &#8220;write in&#8221; the accidentals that are specified by the key
            signature to make it easier to remember to play them. These are
            &#8220;written in&#8221; in blue in the next image:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="An excerpt from Franz Liszt's Hungarian Rhapsody 2 with accidentals written in, in blue ink"
              src="/wp-content/uploads/2013/03/LisztLine1TN2Blue.gif"
              width="656"
              height="186"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            Even stripped down to just the basics of notes and accidentals the
            passage is still quite challenging for a beginner to read. Almost
            every note is modified by the key signature or an accidental sign.
            But is the music really as difficult as it appears?
          </p>
          <p>&nbsp;</p>
          <p>
            <audio preload="auto" controls
              ><source src="/audio/liszt/liszt.oga" type="audio/ogg" /><source
                src="/audio/liszt/liszt.mp3"
              />
            </audio>
          </p>
          <p>&nbsp;</p>
          <p>
            Listen to the audio clip and notice how the notes actually move in a
            fairly simple and straightforward manner, always moving by semitone
            intervals up and down the chromatic scale. The music is actually
            much simpler than it looks in traditional notation.
          </p>
          <p>
            Let&#8217;s see how much clearer this passage appears in some
            notations with a <a href="/">chromatic staff</a>. First let&#8217;s
            take <a href="/systems/6-6-tetragram-by-richard-parncutt/"
              >6-6 Tetragram</a
            > notation by Richard Parncutt:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Parncutt's 6-6 Tetragram Notation"
              src="/wp-content/uploads/2013/03/LisztLine1P66.gif"
              width="675"
              height="223"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            With a <a href="/">chromatic staff</a> notation, each note has its own
            unique place on the staff so there is no need to refer to accidentals
            or a key signature to determine the pitch of the notes.
          </p>
          <p>
            Now it becomes clear that the music itself is not nearly as
            complicated as it first appeared. We can see that it is a fairly
            straightforward chromatic progression of the same chords and
            intervals. The bass or left hand part is a series of octaves. Notice
            how the notes an octave apart have the same appearance on the staff.
            Similarly, in the treble or right hand part we can see a series of
            chords all consisting of an octave and a major third.
          </p>
          <p>
            These basic advantages of a chromatic staff are present in the
            following examples as well. Next, let&#8217;s take a look at (an
            approximation of) <a
              href="/systems/isomorph-notation-by-tadeusz-wojcik/">Isomorph</a
            > notation by Tadeusz Wójcik:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Isomorph Notation by Tadeusz Wojcik"
              src="/wp-content/uploads/2013/03/LisztLine1ISO3.gif"
              width="671"
              height="243"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            You may have noticed that in 6-6 Tetragram notation (above) the
            notes of this chromatic progression alternated between lines and
            spaces. Here in Isomorph they alternate between solid/black
            noteheads and hollow/white noteheads. This makes it even clearer
            that the notes are ascending or descending in semitone (half-step)
            intervals. This feature of these two notations is a result of their
            both having a <a href="/tutorials/6-6-and-7-5-pitch-patterns/"
              >6-6 pitch pattern</a
            > &mdash; found in the line pattern of 6-6 Tetragram, and in the noteheads
            of Isomorph.
          </p>
          <p>
            Isomorph shares several similarities with Klavar notation. The notes
            fall on opposite sides of the stems depending on whether they are
            black or white. Also, it uses a proportional rhythmic notation, so
            the beams used with the eighth notes in traditional notation are
            optional. Next, let&#8217;s take a look at (a close approximation
            of) <a href="/systems/panot-notation-by-george-skapski/">Panot</a> notation
            by George Skapski:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Panot Notation by George Skapski"
              src="/wp-content/uploads/2013/03/LisztLine1Panot.gif"
              width="671"
              height="230"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            And (a close approximation of) the <a
              href="/systems/untitled-by-johannes-beyreuther/"
              >Untitled notation</a
            > by Johannes Beyreuther:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Untitled Notation by Johannes Beyreuther"
              src="/wp-content/uploads/2013/03/LisztLine1Beyr.gif"
              width="672"
              height="246"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            These notations share the same line pattern, and are visually less
            dense or &#8220;busy&#8221; than the notations above. Like Isomorph,
            Beyreuther&#8217;s notation also has a <a
              href="/tutorials/6-6-and-7-5-pitch-patterns/">6-6 pattern</a
            > in its notehead color that makes it easy to see the music ascending
            and descending by semitones. Next, let&#8217;s look at <a
              href="/systems/twinline-notation-by-thomas-reed/">Twinline</a
            > notation by Thomas Reed:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Twinline Notation by Thomas Reed"
              src="/wp-content/uploads/2013/03/ListLine1Twinline.gif"
              width="650"
              height="211"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            Twinline also has a <a href="/tutorials/6-6-and-7-5-pitch-patterns/"
              >6-6 pitch pattern</a
            > that is present in its notehead shapes, so you can see the notes alternating
            from triangles to ovals indicating semitone (half-step) intervals. The
            use of two notehead shapes allows the Twinline staff to take up less
            vertical space. Next, let&#8217;s look at some chromatic notations that
            have a <a href="/tutorials/6-6-and-7-5-pitch-patterns/"
              >7-5 pitch pattern</a
            >, starting with <a
              href="http://http://musicnotation.org/systems/klavar-mirck-version-by-jean-de-buur/"
              >Klavar, Mirck Version</a
            > by Jean de Buur:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Klavar, Mirck version by Jean de Buur"
              src="/wp-content/uploads/2013/03/LisztLine1Mirck4.gif"
              width="668"
              height="279"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            Klavar has a <a href="/tutorials/6-6-and-7-5-pitch-patterns/"
              >7-5 pitch pattern</a
            > in its noteheads and line pattern. This means that all of the notes
            that are naturals or double sharps in traditional notation are open/white
            noteheads and appear on spaces on the right side of the stem, and those
            that are sharps are solid/black and appear on lines on the left side
            of the stem. In the Mirck version of Klavar, notes appear on the right
            side of the stem unless they are in a chord that consists of both black
            and white notes, then they follow the usual Klavar placement.
          </p>
          <p>
            Klavar uses a proportional rhythmic notation, so the notes are
            spaced evenly along the horizontal axis and measure lines coincide
            with the stems of the note on the first beat of each measure. The
            beams are optional. (Note that standard Klavar is usually oriented
            vertically and read from the top to the bottom of the page. This
            illustration of the Mirck version is oriented horizontally instead.)
            Next let&#8217;s look at <a
              href="/systems/express-stave-by-john-keller/">Express Stave</a
            > by John Keller:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="Excerpt from Franz Liszt's Hungarian Rhapsody 2 in Express Stave by John Keller"
              src="/wp-content/uploads/2013/03/LisztLine1ES.gif"
              width="650"
              height="205"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            Express Stave also has a <a
              href="/tutorials/6-6-and-7-5-pitch-patterns/">7-5 pitch pattern</a
            > in its noteheads. All of the notes that are naturals or double sharps
            in traditional notation are open/white noteheads, and those that are
            sharps are solid/black. The staff takes up less vertical space because
            the notes are not spaced as widely apart on the vertical axis as in most
            other chromatic staff systems. The black and white noteheads help to
            distinguish the pitches of notes that are vertically close together.
          </p>
          <p>&nbsp;</p>
          <p>
            In sum, traditional notation with its key signatures and accidental
            signs makes this relatively simple passage of music more difficult
            than it needs to be. In contrast, chromatic staff notations make it
            much easier to read, play, and understand. The 6-6 chromatic
            notations in particular reveal the passage&#8217;s interval
            structure much more clearly. The 7-5 chromatic notations show the
            white and black notes clearly to the pianist, a tablature-like
            benefit. These advantages free up the attention of the musician to
            concentrate on other articulation signs and overall musicality.
          </p>
          <p>
            Thanks goes to Andre Lippens for pointing out this passage from
            Liszt&#8217;s works. If you know of a difficult passage in
            traditional notation that you would like to suggest as another
            instructive example, please <a
              href="/home/about-faq-contact-info/#contact">contact us</a
            >.
          </p>
          <div class="divider-section footnotes">
            With each of these notation systems the passage could also be
            displayed using a &#8216;continuous&#8217; staff option which would
            reduce the amount of vertical space that they would require. For the
            purposes of this tutorial we have followed the convention of
            traditional notation in using a divided staff, one staff each for
            the treble and bass parts.
          </div>
          <p>
            One may object that since this is basically a chromatic passage of
            music, a chromatic staff notation will obviously be more suited for
            it. This is true enough, but it was chosen as a worst-case scenario,
            a limit case that dramatically illustrates a general principle.
          </p>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a
              href="/tutorials/enharmonic-equivalents/"
              rel="prev"
              class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Enharmonic Equivalents</a
            >
          </p>
          <p class="tut-nav-previous">
            <a
              href="/tutorials/intervals-in-6-6-music-notation-systems/"
              rel="next"
              class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: Intervals in 6-6 Music
              Notation Systems</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>

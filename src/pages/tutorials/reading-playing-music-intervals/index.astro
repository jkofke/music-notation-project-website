---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle(
    "Reading and Playing Music by Intervals",
    "Tutorials"
  )}
  metaDescription="A discussion of the role and importance of the representation of intervals in music notation systems."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Reading and Playing Music by Intervals</h1>
        </header>

        <div class="entry-content">
          <p>
            <em
              >Clearly a music notation system should make it easy to identify
              individual notes.  This tutorial discusses why it is also
              important to make it easy to identify intervals as well. For
              example, playing by ear, improvising, and transposing all rely on
              hearing the intervals in music, but traditional music notation
              hinders the identification of intervals.  Alternative notation
              systems can solve this problem. This tutorial is part of a series
              that includes <a
                title="Intervals in Traditional Music Notation"
                href="/tutorials/intervals/"
                >Intervals in Traditional Music Notation</a
              > and <a
                title="Intervals in 6-6 Music Notation Systems"
                href="/tutorials/intervals-in-6-6-music-notation-systems/"
                >Intervals in 6-6 Music Notation Systems</a
              >.</em
            >
          </p>
          <h2 class="divider-heading">
            Reading Music by Individual Notes and by Intervals
          </h2>
          <p>
            <img
              loading="lazy"
              class="alignnone wp-image-2023 size-full"
              src="/wp-content/uploads/2014/06/reading-by-notes-and-intervals.png"
              alt="reading-by-notes-and-intervals"
              width="411"
              height="107"
              srcset="/wp-content/uploads/2014/06/reading-by-notes-and-intervals.png 411w, /wp-content/uploads/2014/06/reading-by-notes-and-intervals-300x78.png 300w"
              sizes="(max-width: 411px) 100vw, 411px"
            />
          </p>
          <p>
            There are two basic ways to read music — by individual notes and by
            intervals (i.e. by absolute orientation and by relative
            orientation). We tend to think of reading music in terms of
            identifying notes by their position on the staff, which is the way
            beginners are usually taught. The staff functions like a map with
            each vertical position corresponding to a particular note name.
          </p>
          <p>
            Music can also be read by recognizing the intervals between notes,
            regardless of their note names. In this case the staff functions
            like a ruler or measuring tape that helps us to see the distances
            between notes which indicates their interval relationship. This goes
            for both melodic intervals and harmonic intervals (notes played both
            sequentially and simultaneously).
          </p>
          <p>
            Usually musicians are not consciously thinking of the names of each
            note or each interval as they play (which would take too much
            time).  They are just reading a note or an interval on the staff and
            directly playing it on their instrument.  The visual notation is
            translated directly into a habitual muscle movement.
          </p>
          <p>
            Musicians may rely on both of these two types of reading when
            playing, so discussing them as separate processes is somewhat
            artificial and only for conceptual clarity.  The main point is that
            they are both useful and complement each other, so it is important
            to consider both of them when assessing a music notation system.
            This is partly because they correspond to two ways of playing an
            instrument.
          </p>
          <p>
            Before moving on, note that reading by interval is only possible to
            a limited extent in traditional music notation.  While it is easy to
            see that the notes shown above are a third apart, whether they are a <em
              >major</em
            > third or a <em>minor</em> third apart is not apparent from their appearance
            alone.  We will return to this in the last section of this tutorial.
          </p>
          <h2 class="divider-heading">
            Playing Music by Individual Notes and by Intervals
          </h2>
          <p>
            <img
              loading="lazy"
              class="alignnone wp-image-2026 size-full"
              src="/wp-content/uploads/2014/06/playing-piano.png"
              alt="playing-piano"
              width="348"
              height="128"
              srcset="/wp-content/uploads/2014/06/playing-piano.png 348w, /wp-content/uploads/2014/06/playing-piano-300x110.png 300w"
              sizes="(max-width: 348px) 100vw, 348px"
            />
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2027"
              src="/wp-content/uploads/2014/06/playing-guitar-frets.png"
              alt="playing-guitar-frets"
              width="348"
              height="108"
              srcset="/wp-content/uploads/2014/06/playing-guitar-frets.png 348w, /wp-content/uploads/2014/06/playing-guitar-frets-300x93.png 300w"
              sizes="(max-width: 348px) 100vw, 348px"
            />
          </p>
          <p>
            Instruments can also be played in these two ways &#8212; by
            individual notes and by intervals (i.e. by absolute orientation and
            by relative orientation). The images above illustrate these two ways
            of playing on a piano keyboard and on a guitar fretboard.
          </p>
          <p>
            Given enough practice most instruments can be played in either way,
            but some instruments lend themselves to being played in one way or
            the other.  The piano is easier to play by individual notes and
            harder to play by interval, due to the irregular key layout. The
            more regular pitch layout of string instruments and <a
              title="Isomorphic Instruments"
              href="/wiki/instruments/isomorphic-instruments/"
              >isomorphic instruments</a
            > make them easier to play by interval. (Note how the difference between
            the major and minor third shown above is clearer on the guitar fretboard
            than on the keyboard.)  When it comes to singing it is generally easier
            to sing by interval than by individual notes (unless you have <a
              href="https://en.wikipedia.org/wiki/Absolute_pitch"
              >&#8220;perfect pitch&#8221; or &#8220;absolute pitch&#8221;</a
            >).
          </p>
          <p>
            There is a complementary relationship between the way one reads and
            plays music.  The way one plays will tend to follow the way one
            reads.  If I read a note (&#8220;F&#8221;) then I will need to know
            how to play that note on my instrument.  If I read an interval
            (&#8220;a third higher than the previous note&#8221;) then I will
            need to know how to play that interval.<a
              href="/tutorials/reading-playing-intervals/#ftn1"
              id="ftnref1">[1]</a
            >
          </p>
          <p>
            As I read and play by notes or by intervals, and I hear those notes
            or intervals, I will be learning both the staff and my instrument in
            terms of either notes or intervals, in a process of repetition and
            reinforcement.  In short, I will be learning to read, play, and hear
            by notes or by intervals.
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2028"
              src="/wp-content/uploads/2014/06/read-play-hear.png"
              alt="read-play-hear"
              width="675"
              height="320"
              srcset="/wp-content/uploads/2014/06/read-play-hear.png 675w, /wp-content/uploads/2014/06/read-play-hear-300x142.png 300w, /wp-content/uploads/2014/06/read-play-hear-500x237.png 500w"
              sizes="(max-width: 675px) 100vw, 675px"
            />
          </p>
          <p>
            If a notation system makes individual notes easy to identify and/or
            intervals easy to recognize then it will facilitate reading <em
              >and playing</em
            > by individual notes and/or by intervals.  Conversely, if a notation
            system makes either of these types of reading more difficult, it will
            make the corresponding type of playing more difficult to learn.
          </p>
          <p>
            Our tutorial on <a
              title="Intervals in Traditional Music Notation"
              href="/tutorials/intervals/"
              >Intervals in Traditional Music Notation</a
            > discusses how traditional music notation makes it difficult to perceive
            intervals because it does not represent them clearly, consistently, or
            directly. It also makes both intervals and individual notes harder to
            read through its use of clefs, key signatures, and accidental signs. 
            The impact of these aspects of traditional notation on how one reads
            music will also affect how one learns to play an instrument.
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2029"
              src="/wp-content/uploads/2014/06/interval-pitch-reading-playing.png"
              alt="interval-pitch-reading-playing"
              width="663"
              height="211"
              srcset="/wp-content/uploads/2014/06/interval-pitch-reading-playing.png 663w, /wp-content/uploads/2014/06/interval-pitch-reading-playing-300x95.png 300w, /wp-content/uploads/2014/06/interval-pitch-reading-playing-500x159.png 500w"
              sizes="(max-width: 663px) 100vw, 663px"
            />
          </p>
          <h2 class="divider-heading">
            Hearing Music by Individual Notes and by Intervals
          </h2>
          <p>
            These two ways of reading and playing also correspond to two ways of
            hearing music. Notes can be heard absolutely as individual notes or
            relatively in terms of their interval relationships to surrounding
            notes.
          </p>
          <p>
            Relatively few people have what is known as <a
              href="https://en.wikipedia.org/wiki/Absolute_pitch"
              >&#8220;perfect pitch&#8221; or &#8220;absolute pitch,&#8221;</a
            > the ability to hear individual notes, completely out of context, and
            identify them by name.  However, most people do have what is known as
            <a href="https://en.wikipedia.org/wiki/Relative_pitch"
              >&#8220;relative pitch,&#8221;</a
            > the ability to hear notes relatively, in terms of their interval relationships
            with other notes.  This allows them to recognize a familiar melody regardless
            of the key in which it is played.  What they are hearing and recognizing
            is the consistent pattern of intervals between the notes (together with
            the rhythm of the notes).
          </p>
          <p>
            Because most people hear music by relative pitch (i.e. interval
            relationships) it makes sense for a notation system to support
            reading by intervals, by making intervals easy to recognize.  This
            allows for an intuitive correspondence between how people hear music
            and the way they read and play it.  On the other hand, a notation
            system that makes it harder to read by intervals leads to a certain
            disconnect or mismatch between how music is heard (interval
            relationships) and how it is read and played (individual notes). 
            This is also closely related to improvising and playing by ear.
          </p>
          <h2 class="divider-heading">Playing by Ear and Improvising</h2>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2030"
              src="/wp-content/uploads/2014/06/hear-play-hear.png"
              alt="hear-play-hear"
              width="675"
              height="204"
              srcset="/wp-content/uploads/2014/06/hear-play-hear.png 675w, /wp-content/uploads/2014/06/hear-play-hear-300x90.png 300w, /wp-content/uploads/2014/06/hear-play-hear-500x151.png 500w"
              sizes="(max-width: 675px) 100vw, 675px"
            />
          </p>
          <p>
            Playing by ear, improvising, and <a
              href="https://en.wikipedia.org/wiki/Transposition_%28music%29#Sight_transposition"
              >transposing by sight</a
            > are skills that involve hearing and playing by intervals and understanding
            music in terms of intervals.  Learning to play by interval when reading
            notation will make it easier to play the interval patterns you hear or
            come up with in your head when playing by ear or improvising.  If a notation
            system makes it easier to read and play by intervals then this will help
            you learn these skills.
          </p>
          <p>
            Often those who play by ear do not read music and those that read
            music may not be able to play by ear.  Perhaps part of the reason is
            that the traditional notation system does not fully support reading
            by interval?  Next we will take a closer look at the limited support
            for reading by intervals in traditional notation.
          </p>
          <h2 class="divider-heading">
            Reading by Intervals in Traditional Notation
          </h2>
          <p>
            As discussed in our tutorial on <a href="/tutorials/intervals/"
              >Intervals in Traditional Music Notation</a
            >, the traditional system makes it difficult to fully perceive the
            intervals between notes: what you see does not consistently
            correspond with what you hear.  This imperfect correspondence makes
            it harder to read and play by interval, to understand music in terms
            of intervals, or even to be aware of the intervals that you are
            playing when reading and playing by individual notes. Nevertheless,
            a certain limited form of reading by interval is still possible in
            traditional notation.
          </p>
          <p>
            On a traditional diatonic staff, it is directly apparent that an
            interval is either a second, third, fourth, fifth, sixth, seventh,
            octave, etc. In music theory this is known as the interval&#8217;s <em
              >name</em
            >. However, an interval&#8217;s appearance does not directly reveal
            its <em>quality:</em>major, minor, perfect, augmented, or
            diminished.
          </p>
          <p>
            This imprecision in the representation of intervals restricts the
            possibilities for reading and playing by intervals. For example, it
            is not much help to know that you need to play a third, if you do
            not also know whether it is a major third (4 semitones) or minor
            third (3 semitones).
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2031"
              src="/wp-content/uploads/2014/06/thirds-change-with-key-signature.gif"
              alt="thirds-change-with-key-signature"
              width="409"
              height="114"
            />
          </p>
          <p>
            To extend this example, it is easy to see that the notes on the
            staff above are ascending by thirds. However, to fully identify them
            and know whether to play a major or minor third, you have to know
            the identity of the individual notes, as determined by the clef
            sign, key signature, and any accidentals. Since you have to know the
            individual notes in order to know what kind of interval to play, it
            appears that reading by interval depends upon reading by individual
            notes and is subordinated to it.
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2051"
              src="/wp-content/uploads/2014/06/thirds-major-or-minor.png"
              alt="thirds-major-or-minor"
              width="482"
              height="139"
              srcset="/wp-content/uploads/2014/06/thirds-major-or-minor.png 482w, /wp-content/uploads/2014/06/thirds-major-or-minor-300x86.png 300w"
              sizes="(max-width: 482px) 100vw, 482px"
            />
          </p>
          <p>
            However, there is more to the story.  Imagine reading and playing
            music in the key of C (with no accidental notes) on an instrument
            that can only play notes in the key of C (like a recorder, a
            harmonica, or a piano without any black keys).  That means that you
            will only be playing notes within <em
              >the set of diatonic notes of the given key</em
            >, both on the staff and on your instrument. (The traditional staff
            was originally invented for just these types of instruments and
            music &#8212; those that are limited to the diatonic notes in one
            key.)
          </p>
          <p>
            Given those constraints, you only need to know an interval&#8217;s
            name (second, third, etc.) in order to play it correctly.  The
            quality of the interval (major, minor, etc.) will be
            &#8220;automatically&#8221; determined by the set of diatonic notes
            in the current key.  Starting from any given note there will only be
            one possible quality for each second, third, fourth, etc. that you
            could play.  For example, a third that starts on the first/tonic
            note in a major key will always be a major third (C to E in the key
            of C), and a third that starts on the third note of the key will
            always be a minor third (E to G).
          </p>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-2053"
              src="/wp-content/uploads/2014/06/diatonic-thirds.png"
              alt="diatonic-thirds"
              width="482"
              height="280"
              srcset="/wp-content/uploads/2014/06/diatonic-thirds.png 482w, /wp-content/uploads/2014/06/diatonic-thirds-300x174.png 300w"
              sizes="(max-width: 482px) 100vw, 482px"
            />
          </p>
          <p>
            What about music that contains &#8220;accidental&#8221; notes,
            chromatic alterations that fall outside of the key?  These
            accidental notes will also fall outside of the scope of this limited
            system of reading by interval.  Accidental signs affect an
            interval&#8217;s quality (not its name), and only an
            interval&#8217;s name is apparent from its direct visual appearance
            on the staff (not its quality).  At best an interval that contains
            one or more accidental notes could be read as an alteration of
            another interval, but it could not be read directly as an interval
            in itself (as it could be on a chromatic staff).
          </p>
          <p>
            So this is the limited form of reading by interval that is possible
            in traditional notation &#8212; reading intervals only by name but
            not by quality.  It depends on musicians having already achieved
            fluency with reading and playing in any given key.  They must have
            already internalized the set of diatonic notes that are represented
            by the staff for a given key, which <span class="draft"
              >entails memorizing all of the key signatures and constantly
              keeping the current key signature in mind when reading music.
            </span>
          </p>
          <p>
            For instruments that can play all twelve chromatic notes per octave,
            the musician must learn to limit himself or herself to only the
            notes in the current key.  That entails memorizing the notes in each
            key and internalizing them through practice.  This is probably one
            reason that practicing scales is emphasized so much.  It is only by <span
              class="draft"
              >ingraining major and minor scales in their fingers and minds that
              musicians can begin to be able to read by interval.</span
            >
          </p>
          <p>
            In short, with traditional notation, reading by interval in any key
            becomes accessible only after a significant amount of memorization
            and practice, and even then it is only possible to a limited
            extent.  This effectively prevents reading by interval from being
            part of the early learning process.
          </p>
          <p>
            How different would it be if reading by interval was fully
            accessible, and easy to do, in any key, right from the start? 
            Instead of rote memorization of key signatures and diatonic scales
            (without a notation system that clearly shows the consistent
            interval patterns found in these scales and keys), musicians could
            easily learn these keys, scales, and interval patterns by seeing
            these patterns directly in the music as they learned to read and
            play it.  (Of course, it will always pay to practice your
            scales&#8230;)
          </p>
          <p>
            For examples of how some chromatic notation systems make it easy to
            read by intervals, see our next tutorial, <a
              href="/tutorials/intervals-in-6-6-music-notation-systems/"
              >Intervals in 6-6 Music Notation Systems</a
            >.
          </p>
          <p>&nbsp;</p>
          <div class="footnotes divider-section">
            <p>
              <a href="/tutorials/reading-playing-intervals/#ftnref1" id="ftn1"
                >[1]</a
              > While one could read notes by their names and then mentally translate
              them into intervals in order to play the intervals (or vice-versa),
              it would be simpler to avoid this extra step of translation. So one&#8217;s
              mode of playing will tend to follow and depend upon one&#8217;s mode
              of reading.
            </p>
          </div>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a
              href="/tutorials/intervals-in-6-6-music-notation-systems/"
              rel="prev"
              class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Intervals in 6-6 Music
              Notation Systems</a
            >
          </p>
          <p class="tut-nav-previous">
            <a href="/tutorials/intervals/" rel="next" class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: Intervals in Traditional
              Music Notation</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>

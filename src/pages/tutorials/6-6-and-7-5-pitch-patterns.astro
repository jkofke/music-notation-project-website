---
import MnpLayout from "../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("6-6 and 7-5 Pitch Patterns", "Tutorials")}
  metaDescription="Learn about the 6-6 and 7-5 pitch patterns, approaches taken by alternative music notation systems that use a chromatic staff. Each offers advantages."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">6-6 and 7-5 Pitch Patterns</h1>
        </header>

        <div class="entry-content">
          <p>
            Many notations that feature a <a href="/">chromatic staff</a> exhibit
            either a 6-6 or a 7-5 pattern in the way that they represent the 12 notes
            of the chromatic scale. These patterns are generally found in the lines
            of a notation&#8217;s staff or in its noteheads.
          </p>
          <h2 class="divider-heading">6-6 Pitch Pattern</h2>
          <p>
            A 6-6 pitch pattern visually distinguishes the two whole-tone scales
            (6 notes each) within the 12 notes of the chromatic scale. This
            regular alternating pattern gives a consistent appearance to
            interval relationships, scales, and chords across all the different
            keys.<a
              href="/tutorials/6-6-and-7-5-pitch-patterns/#ftn1"
              id="ftnref1">[1]</a
            > (See our tutorial <a
              href="/tutorials/intervals-in-6-6-music-notation-systems/"
              >Intervals in 6-6 Music Notation Systems</a
            >). Here are some examples of notations with a 6-6 pattern:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="6-6 Pattern Caption"
              src="/wp-content/uploads/2013/03/66barBtopFFF.png"
              width="600"
              height="67"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            1. 6-6 Line Pattern:<br />
            <a href="/systems/6-6-tetragram-by-richard-parncutt/"
              ><img
                loading="lazy"
                alt="6-6 Pattern on Parncutt's Tetragram Notation"
                src="/wp-content/uploads/2013/03/66Parncutt7b.png"
                width="600"
                height="65"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            2. 6-6 Notehead Color:<br />
            <a href="/systems/untitled-by-johannes-beyreuther/"
              ><img
                loading="lazy"
                alt="6-6 Pattern on Parncutt's Tetragram Notation"
                src="/wp-content/uploads/2013/03/66Beyreuther7.png"
                width="600"
                height="56"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            3. 6-6 Notehead Shape:<br />
            <a href="/systems/twinline-notation-by-thomas-reed/"
              ><img
                loading="lazy"
                alt="6-6 Pattern on Parncutt's Tetragram Notation"
                src="/wp-content/uploads/2013/03/66Twinline7.png"
                width="600"
                height="33"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            4. 6-6 Notehead Color &amp; Line Pattern:<br />
            <a href="/systems/da-notation-by-rich-reed/"
              ><img
                loading="lazy"
                alt="6-6 Pattern on DA Notation by Rich Reed"
                src="/wp-content/uploads/2013/03/66RichReed7b.png"
                width="600"
                height="66"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="6-6 Pattern Caption"
              src="/wp-content/uploads/2013/03/66barBbottomFFF.png"
              width="600"
              height="46"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            These notation systems are: (1) <a
              href="/systems/6-6-tetragram-by-richard-parncutt/"
              >6-6 Tetragram by Richard Parncutt</a
            >, (2) <a href="/systems/untitled-by-johannes-beyreuther/"
              >Untitled by Johannes Beyreuther</a
            >, (3) <a href="/systems/twinline-notation-by-thomas-reed/"
              >Twinline, Thomas Reed Version</a
            >, (4) <a href="/systems/da-notation-by-rich-reed/"
              >DA Notation by Rich Reed</a
            >
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">7-5 Pitch Pattern</h2>
          <p>
            A 7-5 pitch pattern retains a visual distinction between the seven
            notes A B C D E F G and the other five notes that must be
            represented using sharp and flat signs in traditional notation. This
            provides continuity with traditional notation, and a correspondence
            with keyboard instruments that exhibit this same 7-5 pattern. Here
            are some examples:
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="7-5 Pattern Caption"
              src="/wp-content/uploads/2013/03/75barBtopFFF.png"
              width="600"
              height="67"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            1. 7-5 Line Pattern<br />
            <a href="/systems/avique-notation-by-anne-and-bill-collins/"
              ><img
                loading="lazy"
                alt="7-5 Pattern on Avique Notation by Collins"
                src="/wp-content/uploads/2013/03/75Collins7b.png"
                width="600"
                height="80"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            2. 7-5 Notehead Color<br />
            <a href="/systems/express-stave-by-john-keller/"
              ><img
                loading="lazy"
                alt="7-5 Pattern on Express Stave by John Keller"
                src="/wp-content/uploads/2013/03/75Express7B.png"
                width="600"
                height="46"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            3. 7-5 Notehead Color &amp; Line Pattern<br />
            <a href="http:///systems/klavar-mirck-version-by-jean-de-buur/"
              ><img
                loading="lazy"
                alt="7-5 Pattern on Collins' Notation"
                src="/wp-content/uploads/2013/03/75Mirck7piano3.png"
                width="661"
                height="56"
              />
            </a>
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              alt="7-5 Pattern Caption"
              src="/wp-content/uploads/2013/03/75barBbottomFFF.png"
              width="600"
              height="46"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            These notation systems are: (1) <a
              href="/systems/avique-notation-by-anne-and-bill-collins/"
              >Avique Notation by Anne &amp; Bill Collins</a
            >, (2) <a href="/systems/express-stave-by-john-keller/"
              >Express Stave by John Keller</a
            >, (3) <a href="/systems/klavar-mirck-version-by-jean-de-buur/"
              >Klavar, Mirck Version by Jean de Buur</a
            >
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">More Examples</h2>
          <p>
            The following PDF files show scales and chords in various notation
            systems that have a 6-6 and 7-5 pitch pattern:
          </p>
          <ul>
            <li>
              <a href="/pdf/comparisons/MajorScales.pdf" target="_blank">
                Major Scale Comparison (PDF)</a
              >
            </li>
            <li>
              <a href="/pdf/comparisons/Triads.pdf" target="_blank">
                Triad Comparison (PDF)</a
              >
            </li>
            <li>
              <a href="/pdf/comparisons/JazzChords.pdf" target="_blank">
                Jazz Chords Comparison (PDF)</a
              >
            </li>
          </ul>
          <p>&nbsp;</p>
          <p>Some notation systems combine both patterns:</p>
          <ul>
            <li>
              Leo de Vries&#8217; <a
                href="/systems/chromatic-twinline-by-leo-de-vries/"
                >Diatonic Twinline</a
              > has a 7-5 notehead color and a 6-6 notehead shape.
            </li>
            <li>
              As shown above, John Keller&#8217;s <a
                href="/systems/express-stave-by-john-keller/">Express Stave</a
              > has a 7-5 notehead color. An <a
                href="/systems/express-stave-by-john-keller/slant"
                >alternative version</a
              > also adds a subtle 6-6 pattern in its notehead shapes by varying
              the slant of the noteheads.
            </li>
            <li>
              Richard Parncutt&#8217;s <a
                href="/systems/groups/whole-step-4-3-lines/">6-6 Tetragram</a
              > (shown above) has a 6-6 line pattern, but it also has a subtle 7-5
              pattern since the three spaces of the four-line staff are F#, G#, and
              A#, and the two ledger lines are C# and D#.
            </li>
          </ul>
          <p>&nbsp;</p>
          <p>
            See the <a href="/systems/">Gallery of Music Notation Systems</a>,
            sorted by 7-5 or 6-6 Pattern for more examples, and the <a
              href="/tutorials/noteheads-and-pitch/">Noteheads and Pitch</a
            > tutorial for more on using noteheads to indicate pitch.
          </p>
          <p>&nbsp;</p>
          <div class="footnotes divider-section">
            <p>
              <a href="/tutorials/6-6-and-7-5-pitch-patterns/#ftnref1" id="ftn1"
                >[1]</a
              > The regular binary alternation of a 6-6 pattern is analogous to the
              distinction between odd and even numbers in mathematics. In both cases
              a distinction between two interlocking sequences (of notes or numbers)
              helps with orientation and with perceiving and measuring relative differences
              between terms (either notes or numbers).
            </p>
          </div>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a
              href="/tutorials/noteheads-and-pitch/"
              rel="prev"
              class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Noteheads and Pitch</a
            >
          </p>
          <p class="tut-nav-previous"></p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>

---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Finale", "Software")}
  metaDescription="Finale music notation software can be used with alternative music notation systems that use a chromatic staff. Find out how this works."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="page type-page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">
            Finale and Alternative Music Notation Systems
          </h1>
        </header>

        <div class="entry-content">
          <p>
            John Keller, a notation designer from Australia, has pioneered a way
            to use <a href="http://www.finalemusic.com/" target="_blank"
              >Finale</a
            > music software with alternative notation systems. His method makes
            it possible to transnotate music into his Express Stave and other alternative
            notation systems. To see the results of his efforts check out the musical
            works we have available for <a
              href="/system/express-stave-by-john-keller/">Express Stave</a
            > which he created with his &#8220;Finale Notation Converter.&#8221;
          </p>
          <p>
            You can see how it works in the following video he created.  See
            more videos on his <a
              href="http://au.youtube.com/user/ExpressStaveNotation"
              target="_blank">YouTube page</a
            >.
          </p>
          <p>
            <iframe
              loading="lazy"
              src="https://www.youtube-nocookie.com/embed/OJGstrBTIE4"
              height="315"
              width="420"
              frameborder="0"></iframe>
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Using Finale with Alternative Music Notation Systems
          </h2>
          <p>
            Using Keller&#8217;s Finale conversion method requires the full
            version of Finale ($600 or $350 academic pricing) or a copy of
            Finale NotePad 2006 or 2007. These earlier versions of NotePad were
            available as a free download until the fall of 2008 when MakeMusic
            pulled them from their site. They did not want them to compete with
            their new NotePad 2009 (which they began selling for $10, along with
            the free Finale Reader that is read-only and offers no editing
            functions &#8211; <a
              href="/blog/2008/09/finale-notepad-2006-get-it/"
              >more details from our blog</a
            >).
          </p>
          <p>
            The method involves pasting custom (alternative) staff styles onto
            existing (traditionally notated) music. Creating the template files
            that contain these custom staff styles requires the full version of
            Finale. However, if you just have NotePad 2006 or 2007 you can still
            use staff styles by copying them from pre-existing template files.
            So NotePad can be used to transnotate music from traditional
            notation into alternative notations, when it is combined with the
            relevant notation template file(s) and possibly a custom music font
            (depending on the alternative notation system). NotePad 2008 and
            later versions do not have this ability to paste staff styles over
            existing music.
          </p>
          <p>
            After you have created a score in an alternative notation in Finale
            NotePad 2006 or 2007, the file can then be imported into a later
            Finale application, in which the original template file itself would
            not work (such as NotePad 2008 or 2009).
          </p>
          <p>
            If you are interested in giving this method a try, please <a
              href="/home/about-faq-contact-info/">contact us</a
            >.
          </p>
          <h2 class="divider-heading">
            Editing music on a chromatic staff in Finale or NotePad 2006
          </h2>
          <p>
            With this method it is also possible to compose music directly in an
            alternative notation, or edit music that has been transnotated. To
            write music, first apply the alternative staff style from a template
            file to an empty staff or staves in Finale or Finale NotePad 2006 or
            2007. If you are using one of the simpler chromatic-staff systems
            where only one note goes in each line or space, you can then use
            your mouse to enter notes by clicking on the chromatic staff just as
            with a traditional staff.
          </p>
          <p>
            There are some complications for notation systems like Express Stave
            or Twinline where more than one note may occupy the same line or
            space (in the Finale staff style). After entering a note on the
            staff in these systems you may then have to use the minus (-) or
            equals (=) keys to scroll through the possible notes for that
            particular line or space to select the correct note.
          </p>
          <p>
            Another limitation is that when dragging a note up or down the
            staff, or using the up and down arrow keys, the note will scroll
            within a particular diatonic scale, rather than scrolling through
            the full chromatic scale. Unfortunately this diatonic scale is not
            necessarily the one that matches the current key, but varies
            depending on the identity of the original note. To correct for this,
            simply apply accidental signs to the note after dragging it.
          </p>
          <p>
            (If the original note is a C D E F G A or B (a white-key note on the
            piano), then it will scroll through that set of notes, the notes of
            the C major scale that do not require an accidental sign. If the
            original note is one that requires an accidental sign (a black-key
            note), then Finale interprets it as a flat.<a
              href="/software/finale/#ftn1"
              id="ftnref1">[1]</a
            > When it scrolls up or down the staff it will remain a flat, moving
            through a diatonic scale of flat notes, a Cb major scale: Cb Db Eb Fb
            Gb Ab Bb. The single exception is an original note of F#/Gb, which Finale
            interprets as F#. This note will remain sharp and scroll through a C#
            major scale: C# D# E# F# G# A# B#.)
          </p>
          <p>
            Other drawbacks when using NotePad 2006 or 2007 are that the clefs
            will not show correctly because there is no way to change the
            standard clefs that come with the default document. Also the detail
            of stem connections will not be quite perfect.
          </p>
          <p>
            Another anomaly is that if you restore the music to traditional
            notation by pasting the &#8220;restore&#8221; staff style, NotePad
            2006 does not always show all the necessary accidentals. To make the
            accidentals appear, you have to copy and paste the music back on to
            itself.
          </p>
          <h3>A slightly different method</h3>
          <p>
            There is a slightly different method that still works with more
            recent versions of NotePad, but it is much less convenient. It
            entails using the full version of Finale to create a file with empty
            custom (alternative) staves, and then pasting existing (traditional)
            music onto the empty staff. The file with the empty alternative
            staves has to match the music that you want to paste into it (in
            terms of pitch range, length of the piece, etc). In the other method
            described above these details are handled automatically, making the
            process much simpler and more convenient.
          </p>
          <div class="footnotes divider-section">
            <p>
              <a href="/software/finale/#ftnref1" id="ftn1">[1]</a> In Finale, if
              you have a chromatic note defined by a midi pitch as they are when
              using percussion maps ( eg midi note 61 is C#/Db ), it uses a default
              system for deciding which enharmonic note to use. By default the &#8220;black-key&#8221;
              notes are all interpreted as flats, except for F#/Gb which is interpreted
              as F#.
            </p>
          </div>
        </div>
      </article>
    </div>
  </div>
</MnpLayout>

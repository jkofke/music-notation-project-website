---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("Wicki", "Wiki")}
  metaDescription="Discussion of the isomorphic Wicki-Hayden note layout used on concertinas, button accordions, and electronic midi instruments. Various color patterns are also explored."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Wicki-Hayden Note Layout</h1>
        </header>

        <div class="entry-content">
          <p>
            The hexagonal Wicki-Hayden note layout has been used for
            concertinas, button accordions, and more recently for electronic
            midi instruments. An isomorphic layout, it follows the same
            principle of <a href="/wiki/music-theory/isomorphism/"
              >isomorphism</a
            > that is found in many alternative music notation systems.
          </p>
          <p>
            <img
              loading="lazy"
              alt="Wicki-Hayden note layout diagram"
              src="/wp-content/uploads/2013/03/Wicki-Hayden.png"
              width="612"
              height="319"
            />
          </p>
          <p>&nbsp;</p>
          <ul>
            <li>
              <a href="http://www.concertina.com/gaskins/wicki/" rel="nofollow"
                >The Wicki System—an 1896 Precursor of the Hayden System</a
              >.
            </li>
            <li>
              <a href="http://thummer.com/" rel="nofollow">Thumtronics</a> (now defunct,
              see its successor <a
                href="http://www.igetitmusic.com/blog/"
                rel="nofollow">iGetIt! Music</a
              > )
            </li>
            <li>
              <a href="http://musicscienceguy.vox.com/" rel="nofollow"
                >Music Science Guy&#8217;s Website</a
              > (Ken Rushton)
            </li>
          </ul>
          <p>&nbsp;</p>
          <h2>Wicki-Hayden Layout on an Axis-49 Midi Instrument</h2>
          <p>
            The image below illustrates a remapping of the <a
              href="http://www.c-thru-music.com/cgi/?page=prod_axis-49"
              rel="nofollow">Axis-49</a
            > (a midi instrument from <a
              href="http://www.c-thru-music.com"
              rel="nofollow">C-Thru Music</a
            >) to the Wicki-Hayden layout. The Axis-49 has been rotated ninety
            degrees from its usual &#8220;wide&#8221; orientation to a
            &#8220;tall&#8221; orientation. This is necessary in order to
            realign the hexagonal button pattern. Ideally, the horizontal rows
            of buttons would then include nine or ten buttons (as shown in the
            image above), rather than just seven. This restricts the number of
            keys in which you can play while using the same fingering patterns
            (and without transposing the instrument).
          </p>
          <p>
            <img
              loading="lazy"
              alt="Axis-49-Wicki-1.png"
              src="/wp-content/uploads/2013/03/Axis-49-Wicki-1.png"
              width="579"
              height="575"
            />
          </p>
          <p>
            The remapping above is &#8220;centered&#8221; in terms of the circle
            of fifths, making it easiest to play in the simplest, most common
            keys. Without changing the basic fingering patterns you can play in
            keys that have as many as three flats, or three sharps. In keys with
            more than three flats or sharps the fingering patterns get
            &#8216;wrapped&#8217; from one side to the other as you run out of
            notes on either side. (If you restrict yourself to the bottom or the
            top half of the layout, you can also play in keys that have 4 flats
            or 4 sharps, respectively, without wrapping fingering patterns.)
          </p>
          <p>
            The color scheme helps to highlight the enharmonically equivalent
            notes on either side of the button-field, by making the A-flats and
            G-sharps blue. Those that sound the same are the same shade of blue,
            either dark or light. This helps with orientation if you have to
            &#8216;wrap&#8217; a fingering pattern to the opposite side.
          </p>
          <p>
            See <a href="http://musicscienceguy.vox.com/" rel="nofollow"
              >Music Science Guy&#8217;s Website</a
            > for more on remapping the Axis-49 to the Wicki-Hayden and other note
            layouts.
          </p>
          <p>&nbsp;</p>
          <h2>More Color Schemes</h2>
          <p>
            Here are some more possibilities for color schemes. (The Axis-49
            comes with 4 colors of buttons, but not in the right numbers to be
            able to recreate these color schemes.)
          </p>
          <h3>Strong 7-5 and Weak 6-6 Color Scheme</h3>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-1441"
              alt="Axis49-Wicki-75-66"
              src="/wp-content/uploads/2013/03/Axis49-Wicki-75-66.png"
              width="352"
              height="580"
              srcset="/wp-content/uploads/2013/03/Axis49-Wicki-75-66.png 352w, /wp-content/uploads/2013/03/Axis49-Wicki-75-66-182x300.png 182w"
              sizes="(max-width: 352px) 100vw, 352px"
            />
          </p>
          <h3>Strong 6-6 and Weak 7-5 Color Scheme</h3>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-1442"
              alt="Axis49-Wicki-66-75"
              src="/wp-content/uploads/2013/03/Axis49-Wicki-66-75.png"
              width="356"
              height="586"
              srcset="/wp-content/uploads/2013/03/Axis49-Wicki-66-75.png 356w, /wp-content/uploads/2013/03/Axis49-Wicki-66-75-182x300.png 182w"
              sizes="(max-width: 356px) 100vw, 356px"
            />
          </p>
          <h3>Major Thirds (3-3-3-3) Color Scheme</h3>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-1443"
              alt="Axis49-Wicki-3333"
              src="/wp-content/uploads/2013/03/Axis49-Wicki-3333.png"
              width="354"
              height="581"
              srcset="/wp-content/uploads/2013/03/Axis49-Wicki-3333.png 354w, /wp-content/uploads/2013/03/Axis49-Wicki-3333-182x300.png 182w"
              sizes="(max-width: 354px) 100vw, 354px"
            />
          </p>
          <h3>Minor Thirds (4-4-4) Color Scheme</h3>
          <p>
            <img
              loading="lazy"
              class="alignnone size-full wp-image-1444"
              alt="Axis49-Wicki-444"
              src="/wp-content/uploads/2013/03/Axis49-Wicki-444.png"
              width="355"
              height="578"
              srcset="/wp-content/uploads/2013/03/Axis49-Wicki-444.png 355w, /wp-content/uploads/2013/03/Axis49-Wicki-444-184x300.png 184w"
              sizes="(max-width: 355px) 100vw, 355px"
            />
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>

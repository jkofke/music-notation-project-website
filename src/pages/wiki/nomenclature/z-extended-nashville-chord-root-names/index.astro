---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("Z", "Wiki")}
  metaDescription="Discussion of Z-Extended Nashville Chord Root Names, a musical nomenclature by David Zethmayr."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Z-Extended Nashville Chord Root Names</h1>
        </header>

        <div class="entry-content">
          <p>
            <img
              loading="lazy"
              alt="SquiggleZM-Nash onwhite@72.png"
              src="/wp-content/uploads/2013/03/SquiggleZM-Nash_onwhite%4072.png"
              width="66"
              height="237"
            />
          </p>
          <p>
            <sub
              ><sub
                >Presentation courtesy of <a
                  href="/wiki/music-theory/the-squiggle-method-of-pitch-visualization/"
                  >Squiggle</a
                > Theory</sub
              >
            </sub>
          </p>
          <h3>Motivation</h3>
          <p>
            &#8220;Nashville&#8221; numbering for chords amounts to a movable
            solfege, in that it is key-free, key-agnostic, key-generalized.
            Studio musicians and multi-instrumentalists typically are familiar
            with using numbers <strong>1..7</strong> as chord root designators, inflected
            as necessary with sharp or flat. What with added degrees and characterizations
            superscripted (min, m, maj, ma, aug, dim, sus, &#8230;) and slash chords,
            this makes for some wordy chord indications.
          </p>
          <p>
            This proposed convention erects five single-letter main designators
            to stand for the inflected forms. It eliminates such complexities as
            &#8220;flatted <strong>.3</strong>&#8221; in <strong>A</strong> not being
            a black keyboard note.
          </p>
          <h3>Z-Modal Music</h3>
          <p>
            Since Nashville departs from Rameau floating modal tonic convention
            (<strong>6</strong> tonic for minor, <strong>5</strong> tonic for mountain,
            <em>e.g.</em>) by treating tonic as <strong>1</strong> regardless of
            mode, the designations <strong>1<sup>mi</sup></strong>, <strong
              >1<sup>m</sup>
            </strong> and <strong>1<sup>&#8211;</sup></strong> for <strong
              >1</strong
            >-minor are seen (minor, dorian, phrygian).
          </p>
          <p>
            In much music, the flatted seventh degree is prominent in practical
            chord-root use. It arises in mountain (mixolydian), dorian and
            natural minor (aeolian), as well as the less frequent phrygian.
            (Lydian and locrian are &#8220;right out&#8221; <sub
              >[Monty Python or somebody]</sub
            >.) Indeed, in folk music such as ballads, sea chanties, and modern
            compositions in their genres, a bi-centric chord structure of tonic
            and sub-tonic is seen in contrast to more modern tri-centric <strong
              >1-4-5</strong
            >. There is a large body of bi-centric music with <strong
              >1-Z</strong
            > (mountain: <em>Old Joe Clark</em>) or <strong
              >1<sup>m</sup>-Z</strong
            > (dorian: <em>Scarborough Fair</em>, <em>Shady Grove</em>, <em
              >Drunken Sailor</em
            >) or <strong>1<sup>m</sup>-Z<sup>m</sup></strong> (phrygian) chord structure.
            The quickly recognizable similarity in sound from this simpler chord
            root alternation comes from their being <em
              ><strong>Z-modal</strong>
            </em>.
          </p>
          <h3>Example</h3>
          <p>
            <img
              loading="lazy"
              alt="Z-Nash Example 1.png"
              src="/wp-content/uploads/2013/03/760px-Z-Nash_Example_1.png"
              width="760"
              height="120"
            />
          </p>
          <p>
            <sub
              >The example is in <a
                href="/wiki/notation-systems/slantnote-by-david-zethmayr/"
                >slantnote-on-rails</a
              >. Rails are tonic and dominant; leger lines likewise. Tonic top
              and bottom is <em>authentic lie</em>, as here. <em>Plagal lie</em>
              puts tonic rail between two dominant rails The &#8220;Three Blind Mice&#8221;
              3-2-1 footprint indicates the mode.</sub
            >
          </p>
          <h3>Mnemonics</h3>
          <p>
            The glyph for <strong>7</strong>-flat is <strong>Z</strong>,
            visually related to the numeral. The other glyphs are likewise
            chosen for mnemonic convenience. The <strong>J</strong> for <strong
              >6</strong
            >-flat/<strong>5</strong>-sharp is a hook mirror-imaging <strong
              >6</strong
            >. The <strong>Q</strong> between <strong>4</strong> and <strong
              >5</strong
            > reflects the Latin words for those numerals (<strong
              >quartus, quintus</strong
            >). Also, imagining <strong>Q</strong> as &#8220;split O&#8221; evokes
            splitting the &#8220;full circle&#8221; <strong>O</strong>ctave
            quite in half, for that &#8220;<strong>Q</strong>ueer&#8221; tritone
            sound.
          </p>
          <p>
            The degree between <strong>2</strong> and <strong>3</strong> is more
            usually related to <strong>3</strong> (blues, <em>e.g.</em>), so <strong
              >M</strong
            > is proposed: <strong>3</strong> rolled over onto its legs (with its
            joints angularized). A further mnemonic hook is that the alteration creates
            a <strong>M</strong>inor tonality. For european-trained musicians, <strong
              >M</strong
            > bespeaks <em>moll</em> (minor). Such a musician is apt to say &#8220;soft&#8221;
            in English instead.
          </p>
          <p>
            For the <strong>2</strong>-flat/<strong>1</strong>-sharp degree I
            chose <strong>T</strong>. &#8220;Tea for Two-flat&#8221; or, for the
            other context, <strong>1</strong> with &#8220;something extra&#8221;
            on it. Constraining these choices was a concern for avoiding conflict
            with musical directions that may likewise appear above a staff, such
            as P, V (up-bow), X (pitchless note), and so on.
          </p>
          <h3>Squiggle Schema Above</h3>
          <p>
            The <a
              href="/wiki/music-theory/the-squiggle-method-of-pitch-visualization/"
              >squiggle diagram</a
            > above, on which these extensions and the <strong>1..7</strong> main
            chord designators are schematized, is the basis for <a
              href="/wiki/notation-systems/slantnote-by-david-zethmayr/"
              >Slantnote</a
            >, an alternative music notation system.
          </p>
          <p>
            &#8212; &#8220;Slantnote&#8221; (David Zethmayr), 22 November 2011
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
